<x-admin-page>

    @push('bootstrap')
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @endpush
    
    @if(session('message'))
    <h1 class="text-success">{{session('message')}}</h1>
    @endif
    <h1>Admin Panel</h1>


    
    @include('admin._users_panel')


    @include('admin._categories_panel')


    @include('admin._games_panel')

</x-admin-page>

<script>


    $(document).ready( function () {
        $('#games_table').DataTable();
        $('#categories_table').DataTable();
        $('#users_table').DataTable();

        if ($("#gameId").val() != 'index') {
            let gameId = "#game-"+$("#gameId").val()+"-modal";
            console.log(gameId);
            $(gameId).modal('show')
        }
    } );
</script>