<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function show(Category $category)
    {
        $games = $category->games;

        // dd($games);

        return view('categories.show', compact('games'));
    }
}
