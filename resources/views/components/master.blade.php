<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js" type="text/javascript"></script> --}}
    <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.js" type="text/javascript"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    
    @stack('bootstrap')
    
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.css">
</head>

<body>

    <header class="py-2 px-6 flex bg-gray-200">
        <h1 class="flex-none text-6xl text-purple-400">
            <a href="{{ route('home') }}">GameBG</a>
        </h1>
        
        <x-search-buddy >
            //search buddy component
        </x-search-buddy>
        
        <div class="flex-grow">
        
        </div>

        @auth
            @if (currentUser()->isAdmin())
                <div>
                    <a href="{{ route('admin.index') }}"><img width="100" src="/images/admin.jpg" alt="admin"></a>
                </div>
            @endif
            
            <div class="flex-none">
                    @include('user-bar')
            </div>
        @endauth
    </header>

 
    <main class="m-8">
        {{ $slot }}
    </main>





    {{-- <script type="text/javascript">
        $(document).ready(function() { 
            $('#users_table').DataTable();
        })
    </script> --}}
    {{-- <script type="text/javascript">
        $(document).ready(function() { 
            $('#myTable').DataTable();
        })
    </script> --}}
</body>


</html>