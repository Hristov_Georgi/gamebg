<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreCategory;
use App\Http\Requests\UpdateCategory;
use Facade\FlareClient\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class CategoryController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    // public function show()
    // {
    //     $categories = Category::all();

    //     dd($categories);

    //     // return View::create('');
    // }

    

    public function store(StoreCategory $request)
    {
        $data = $request->validated();

        // $data = $request->validate([
        //     'name' => ['required','min:3','max:25','string','unique:categories'],
        //     'image' => ['image','required']
        // ]);

        // dd($request->file('image')->store('files/categories'));
        
        $image = $data['image']->store('categories/images');

        
        $category = new Category();
        
        $category->name = $data['name'];
        $category->image = '/storage/' . $image;
        $category->slug = Str::slug($data['name'],'-');

        $category->save();

        // dd('ok');

        return back()->with('message','Category added successfuly');

    }

    public function update(UpdateCategory $request, Category $category)
    {

        // $data = $request->validate([
        //     'name' => ['required','min:3','max:25'],
        //     'image' => ['image']
        // ]);

        $data = $request->validated();

        $category->name = $data['name'];

        if ($request->has('image')) {
            $image = $data['image']->store('/files/categories');

            $url = str_replace('/storage/','',$category->image);
            Storage::delete($url);

            $category->image = '/storage/' . $image;
        }
        $category->save();

        return back()->with('message', 'Category edited seccessfuly');
    }

    public function destroy(Category $category)
    {
        $category->delete();      
        
        return  back();
    }   


}
