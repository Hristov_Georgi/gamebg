<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role  = new Role();
        $role->name = "administrator";
        $role->save();

        $role = App\Role::first();
        $user = User::where('username', 'admin')->get()[0];
        $user->role()->attach($role->id);
    }
}
