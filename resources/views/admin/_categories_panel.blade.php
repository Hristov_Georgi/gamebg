<h1>Categories</h1>


    <div>
        <table id="categories_table" class="display">
            <thead>
                <tr>
                    <th>Category Cover</th>
                    <th>Name</th>
                    <th>Created On</th>
                    <th>Last Updated</th>
                    <th>Games</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($categories as $category)
                <tr>
                    <td class="d-flex justify-content-center">
                        <img width="50" src="{{ $category->image }}" alt="">
                    </td>
                    <td>{{ $category->name }}</td>
                    @if ($category->created_at && $category->updated_at)
                        <td>{{ $category->created_at->diffForHumans() }}</td>
                        <td>{{ $category->updated_at->diffForHumans() }}</td>
                    @else
                        <td>{{ $category->created_at }}</td>
                        <td>{{ $category->updated_at }}</td>
                    @endif
                    <td><a href="{{ route('categories.show', $category) }}">
                        {{ $category->games()->count() }}
                        </a>
                    </td>
                    <td>
                        
                        

                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#category-{{$category->id}}-modal">
                                Edit
                            </button>
                                            
                                            <!-- Modal -->
                            <div class="modal fade" id="category-{{$category->id}}-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Edit category data</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    </div>
                                    <div class="modal-body">
                                        <form enctype="multipart/form-data" id="category-{{ $category->id}}" method="POST" action="{{ route('admin.categories.update', $category) }}">
                                            @csrf
                                            @method('put')
                                            <label for="name">Change category name</label>
                                            <input type="text" name="name" value="{{ $category->name }}" class="border border-primary rounded" >
                                            <br>
                                            <label for="image">Change category image</label>
                                            <input type="file" name="image" >
                                        
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                    <button type="submit" form="category-{{$category->id}}" class="btn btn-primary">Edit Category</button>
                                    </div>
                                </div>
                                </div>
                            </div>

                    

                    </td>
                    <td>
                        <form  method="POST" action="{{ route('admin.categories.destroy',$category) }}">
                            @csrf
                            @method('delete')
                            <button type="submit">DELETE</button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <!-- Button trigger modal -->
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createCategoryModal">
        Create New Category
    </button>
    
    <!-- Modal CREATE CATEGORY -->
    <div class="modal fade" id="createCategoryModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Enter data</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <form enctype="multipart/form-data" id="category_form" method="POST" action="{{ route('admin.categories.store') }}">
                    @csrf
                    <label for="n">Enter category name</label>
                    <input type="text" name="name" id="n" class="border border-primary" >
                    <br>
                    <label for="i">Upload an image</label>
                    <input type="file" name="image" id="i" >
                   
                </form>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            <button type="submit" form="category_form" class="btn btn-primary">Make Category</button>
            </div>
        </div>
        </div>
    </div>