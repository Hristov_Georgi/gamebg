<x-profile>    
    <div class="flex">
        @forelse ($users as $user)
            <div>
                <a href="{{ route('profiles.show', $user) }}">
                    <p>{{ $user->username }}</p>
                    <img src="{{ $user->avatar }}" 
                    width="50" alt="">
                </a>
                <a href="{{ route('messages.show', $user) }}">See chats</a>
            </div>
        @empty
            <h1>No conversations yet</h1>
        @endforelse
    </div>
</x-profile>