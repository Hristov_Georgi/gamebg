<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateCategory extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $currentCategory = $this->route('category');

        return [
                'name' => ['required','min:3','max:25',Rule::unique('categories')->ignore($currentCategory)],
                'image' => ['image']
        ];
    }
}
