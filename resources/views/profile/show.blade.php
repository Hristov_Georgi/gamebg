<x-profile>
            @if (session('message'))
            <div class="text-lg text-green-500">
                <p>{{ session('message') }}</p>
            </div>
            @endif
            
            <h1>{{ $user->name }}'s profile</h1>

          
            @if (currentUser()->is($user))
                <div><a href="{{ route('profiles.edit', auth()->user()) }}">Edit your profile DATA</a></div>
                <div><a href="{{ route('user-game-lib', auth()->user()) }}">Your Game library</a></div>
                
                <div>
                    <a href="{{ route('friends.index') }}">My Friends</a>
                </div>

                <div>
                    <a href="{{ route('messages.index') }}">Messages</a>
                </div>
            @else
                <div>
                    <a href="{{ route('messages.show', $user->username) }}">Send Message To {{ $user->username }}</a>
                </div>
                               
                @if (currentUser()->hasFriend($user))
                    <form action="{{ route('friends.destroy', $user->username) }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button type="submit">Remove friend</button>
                    </form>
                    
                @else
                    <form action="{{ route('friends.store') }}" method="POST">
                        @csrf
                        <input type="hidden" name="username" value="{{ $user->username }}">
                        <button type="submit">Add Friend</button>
                    </form>
                @endif


                <div><a href="{{ route('user-game-lib', $user->username) }}">Check {{ $user->username }}'s library</a></div>


            @endif

</x-profile>