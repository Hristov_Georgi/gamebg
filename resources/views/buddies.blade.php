<x-master>
    @forelse ($users as $key => $user) 
        
        <p>user: <a href="{{ route('profiles.show',$user->username) }}"></a>{{ $user->username }}</p>
        <img width="50" src="{{ $user->avatar }}" alt="">
        <p>matching searches: 
            <ul>
                @foreach ($user->games as $game)
                    
                    @if ($matches->contains($game->name))
                        <a href="{{ route('game', $game->id) }}"><li>{{ $game->name }}</li></a>
                    @endif

                @endforeach
            </ul>
        </p>
    @empty
        <p>No users matched your search</p>
    @endforelse
</x-master>