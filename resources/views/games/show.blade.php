<x-master>
    <div>
        <div>
            @if (isset($gameCoverImg))
            <img src="{{ $gameCoverImg }}"   
            alt="{{ $game->name }}'s cover'">
            @endif
        </div>

        <div>
            <h1>Game title - {{ $game->name }}</h1>
            <p>Game Descrioption - {{ $game->description }}</p>
        </div>
    </div>
</x-master>