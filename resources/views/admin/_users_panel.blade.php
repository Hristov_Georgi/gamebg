<h1>Users</h1>
<div>
    <table id="users_table" class="display">
        <thead>
            <tr>
                <th>Name</th>
                <th>Username</th>
                <th>Email</th>
                <th>Registered On</th>
                <th>Last Updated</th>
                <th>Games playing</th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach ($users as $user)
            <tr>
                <td>{{ $user->name }}</td>
                <td>{{ $user->username }}</td>
                <td>{{ $user->email }}</td>
                @if ($user->created_at && $user->updated_at)
                        <td>{{ $user->created_at->diffForHumans() }}</td>
                        <td>{{ $user->updated_at->diffForHumans() }}</td>
                    @else
                        <td>{{ $user->created_at }}</td>
                        <td>{{ $user->updated_at }}</td>
                    @endif
                <td class="text-center"><a href="{{ route('user-game-lib', $user->username) }}">{{ $user->games()->count()}}<a></td>
                <td>
                    <a href="{{ route('profiles.show', $user) }}">VIEW</a>
                </td>
                <td>
                    <form method="POST" action="{{ route('admin.users.destroy', $user->id) }}">
                        @csrf
                        @method('DELETE')
                        <button type="submit">DELETE</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>


    