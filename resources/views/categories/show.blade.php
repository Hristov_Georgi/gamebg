<x-master>
    @forelse ($games as $game)
        <div class="">
            <h2>Title-> {{ $game->name }}</h2>
            
            <x-display-image :game=$game></x-display-image>
        </div>
    @empty
        <h1>No games added to this category yet</h1>
    @endforelse
</x-master>