<x-profile>
    <form method="POST" enctype="multipart/form-data" action="{{ route('profiles.update',auth()->user()) }}">
        @csrf
        @method('PUT')
        <div class="nb-6">
            <label class="block mb-2 uppercase font-bold text-xs text-gray-600" for="name">
                Name
            </label>
            <input class="border border-gray-400 p-2 w-full" type="text" id="name" name="name"
                value="{{ $user->name }}">

            @error('name')
            <p class="text-red-500 text-xs">{{ $message }}</p>
            @enderror
        </div>


        <div class="nb-6">
            <label class="block mb-2 uppercase font-bold text-xs text-gray-600" for="username">
                Username
            </label>
            <input class="border border-gray-400 p-2 w-full" type="text" id="username" name="username"
                value="{{ $user->username }}">

            @error('username')
            <p class="text-red-500 text-xs">{{ $message }}</p>
            @enderror
        </div>



        <div>
            <div class="nb-6">
                <label class="block mb-2 uppercase font-bold text-xs text-gray-600" for="avatar">
                    Avatar
                </label>
                <input class="border border-gray-400 p-2 w-full" type="file" name="avatar"

                @error('avatar')
                <p class="text-red-500 text-xs">{{ $message }}</p>
                @enderror
            </div>
            <img src="{{ $user->getAvatar() }}" alt="">
        </div>



        <div class="nb-6">
            <label class="block mb-2 uppercase font-bold text-xs text-gray-600" for="email">
                Email
            </label>
            <input class="border border-gray-400 p-2 w-full" type="email" id="email" name="email"
                value="{{ $user->email }}">

            @error('email')
            <p class="text-red-500 text-xs">{{ $message }}</p>
            @enderror
        </div>


        <div class="nb-6">
            <button class="bg-red-400 text-white rounded py-2 px-4 mt-2" type="submit">
                <a href="{{ url()->previous() }}">Cancel</a>
            </button>

            <button class="bg-blue-400 text-white rounded py-2 px-4 mt-2" type="submit">
                Submit
            </button>


        </div>
    </form>
    
    <a class="bg-red-700 text-white rounded py-2 px-4 mt-2" 
    href="{{ route('edit-pass') }}">
    Change Password
    </a>


</x-profile>