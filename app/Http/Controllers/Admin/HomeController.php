<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Game;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    public function __invoke()
    {

        $users = User::with('games')->get();
        $categories = Category::with('games')->get();
        $games = Game::with('images','category')->get();


        return view('admin.index', compact('users','games','categories'));
    }
}
