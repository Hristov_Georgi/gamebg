<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Category;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Category::class, function (Faker $faker) {

    $name = $faker->sentence(1);

    return [
        'name' => $name,
        'slug' => Str::slug($name,'-'),
        'image' => '/storage/categories/images/' . $faker->file(            //make cross OS paths
            'public/app_files/game_categories',
            'storage/app/public/categories/images',
            false
        )
    ];
});
