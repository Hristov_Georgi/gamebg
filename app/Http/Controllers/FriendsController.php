<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class FriendsController extends Controller
{
    public function index()
    {
        $user = currentUser();
        return view('friends', compact('user'));
    }


    public function store()
    {

        $request = request()->username;
        
        $user = User::where('username', '=', $request)->first();  // with hidden input field
    

        if (!currentUser()->hasFriend($user)) {

            currentUser()->addFriend($user);
        }
 

        return back();
    }

    public function destroy(User $friend)
    {
     
        $user = $friend;
        if (currentUser()->hasFriend($user)) {

            currentUser()->removeFriend($user);
        }

        return back();
    }
}
