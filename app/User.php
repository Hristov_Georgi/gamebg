<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;


class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'email', 'password', 'avatar'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function getAvatar()
    {
        $avatar = null;
        return $avatar;
    }


    public function getRouteKeyName()
    {
        return 'username';
    }

    public function path($append = '')
    {
        $path = route('profiles.show', $this->username);
        return $path . '/' . $append;
    }


    ////////////GAME RELATED

    public function games()
    {
        return $this->belongsToMany(Game::class, 'games_users')->withTimestamps();
    }


    public function attachGame(Game $game)
    {
        // return $this->games()->attach($game);
        // return $this->games()->syncWithoutDetaching($game);
        return $this->games()->sync($game, false);
    }


    public function detachGame(Game $game)
    {
        return $this->games()->detach($game);
    }


    public function isPlaying(Game $game)
    {

        $gameTitle = $game->name;

        return $this->games()->where('name', '=', $gameTitle)->exists();
    }

    ////////////FRIEND RELATED

    public function friends()
    {
        return $this->belongsToMany(User::class, 'friends', 'user_id', 'friend_id');
    }


    public function hasFriend(User $user)
    {
        return $this->friends()->where('username', '=', $user->username)->exists();
    }


    public function addFriend(User $user)   // may implement toggle
    {
        return $this->friends()->attach($user->id);
    }

    public function removeFriend(User $user)
    {
        return $this->friends()->detach($user->id);
    }


    ////////////MESSAGE RELATAED




    public function getConversation(User $user)
    {
        //SELECT * FROM `messages` WHERE ( sender_id = 1 AND receiver_id = 2) OR ( sender_id = 2 AND receiver_id = 1 )

        return Message::where('sender_id', $this->id)->where('receiver_id', $user->id)->orWhere('sender_id', $user->id)->where('receiver_id', $this->id)->get();
    }/*  */


    public function allMessages()
    {
        return $this->hasMany(Message::class, 'sender_id')->orWhere('receiver_id', $this->id);
    }



    public function getConversationUsers()
    {

        $sender = $this->allMessages()->pluck('sender_id');
        $receiver = $this->allMessages()->pluck('receiver_id');

        $merged = $sender->merge($receiver);


        $filtered = $merged->filter(function ($value, $key) {
            return $value != $this->id;
        });

        $unique = $filtered->unique();
        $unique->values()->all();


        return User::whereIn('id', $unique)->get();
    }



    public function messagesTo()
    {
        return $this->hasMany(Message::class, 'sender_id');
    }

    public function messagesFrom()
    {
        return $this->hasMany(Message::class, 'receiver_id');
    }



    //AUTH 


    public function role()
    {
        return $this->belongsToMany(Role::class, 'roles_users', 'user_id', 'role_id')->withTimestamps();
    }


    public function isAdmin()
    {
        return $this->role()->where('name', 'administrator')->exists();
    }
}
