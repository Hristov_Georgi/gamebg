<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Message;
use App\User;
use Faker\Generator as Faker;

$factory->define(Message::class, function (Faker $faker) {
    return [
        'sender_id' => User::orderByRaw('RAND()')->first()->id,
        'receiver_id' => User::orderByRaw('RAND()')->first()->id,
        'message' => $faker->text(50),
    ];
});
