@if ($game->getMainImage())
        <a href="{{ route('games.show', $game) }}">
        <img 
        width="100"
        src="{{ $game->getMainImage()->path }}"
        alt="">
        </a>
@else
    {{-- fallback --}}
    <a href="{{ route('games.show' ,$game) }}">
    <img width="100" src="/images/noimg.png" alt="cover image">
    </a>
@endif