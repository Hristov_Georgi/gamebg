<x-master>
    <div class="grid grid-cols-7 gap-10">
        @forelse ($games as $game)
        <div class="bg-gray-400">
            @auth
                @if (currentUser()->isPlaying($game))
                    <form action="{{ route('del-game', $game) }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button type="submit">
                            I no longer play this-
                        </button>
                    </form>
                @else
                    <form action="{{ route('add-game', $game) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <button type="submit">
                            I play this+
                        </button>
                    </form>
                @endif

                <p><a href="#">People who play this game</a></p>
            @endauth
            <h2>{{ $game->name }}</h2>

            <a href="{{ route('games.show', $game) }}">
                <x-display-image :game=$game></x-display-image>
            </a> 
            <p>{{ $game->desription }}</p>
            
        </div>
        @empty
            <h2>No games added</h2>
        @endforelse
    </div>

    <div class="mt-10 flex justify-center">
        {{ $games->links() }}
    </div>
</x-master>