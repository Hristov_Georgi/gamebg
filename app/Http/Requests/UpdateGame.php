<?php

namespace App\Http\Requests;

use App\Game;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateGame extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $currGameName = $this->route('game');
        
        return [
            'name' => ['min:3','max:30','required',Rule::unique('games')->ignore($currGameName)],
            'description' => ['min:4','max:255','required'],
            'categories' => ['required', 'numeric', Rule::exists('categories','id')],
            // 'image' => ['image'],
        ];
    }
}
