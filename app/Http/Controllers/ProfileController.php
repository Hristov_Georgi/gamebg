<?php

namespace App\Http\Controllers;


use App\Game;
use App\Http\Requests\UpdateUserPassword;
use App\Http\Requests\UpdateUserProfile;
use App\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('profile.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $profile)
    {
        $user = $profile;
        return view('profile.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $profile)
    {
        
        $user = $profile;
        return view('profile.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserProfile $request)
    {

        $data = $request->validated();

        if ($request->has('avatar')) { 
            $avatarPath = $request->file('avatar')->store('avatars');
            $data['avatar'] = '/storage/' . $avatarPath;
        } else {
            //current user
        }

        $isUpated = currentUser()->update($data) ? true : false;

        return redirect(currentUser()->path());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function editPassword()
    {
        return view('profile.edit-password');
    }

    public function updatePassword(UpdateUserPassword $request, User $user)
    {

        $data = $request->validated();
     

        $data['newPass'] = bcrypt($data['newPass']);

        if (Hash::check($data['currentPass'], $user->password)) {

            $user->password = $data['newPass'];

            $user->save();

            return redirect()->route('profiles.show', $user->username)->with('message', 'You have successfuly changed your credentianls!');
        }

        return back()->with('message', 'Your current password isn\'t correct');
    }


    public function addGame(Game $game)
    {

        currentUser()->attachGame($game);

        return redirect()->route('games.index');
    }

    public function removeGame(Game $game)
    {
        currentUser()->detachGame($game);

        // return redirect()->route('user-game-lib', currentUser());
        return back();
    }
}
