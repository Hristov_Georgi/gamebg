<?php

namespace App\Http\Middleware;

use Closure;

class AdministratorAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!currentUser()->isAdmin() /* && currentUser() */) {
            return abort(403,'You are not an administrator!');
        }
        return $next($request);
    }
}
