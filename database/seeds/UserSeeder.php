<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        
        DB::table('users')->insert([
            'name' => 'Admin Adminov',
            'username' => 'admin',
            'email' => 'admin@admin.com',
            'password' => Hash::make('123456'),
            'avatar' => '/storage/avatars/' . $faker->file(           
                'public/app_files/avatars',
                'storage/app/public/avatars',
                false)
        ]);


        $user = new User;
        $user->name = 'Admin The Second';
        $user->username = 'admin2';
        $user->email = 'admin2@admin.com';
        $user->password = bcrypt(123456);
        $user->avatar = '/storage/avatars/' . $faker->file(           
            'public/app_files/avatars',
            'storage/app/public/avatars',
            false);

        
        $user->save();

    }
}
