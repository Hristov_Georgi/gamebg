<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function images()
    {
        return $this->hasMany(Image::class);
    }

    public function user()
    {
        return $this->belongsToMany(User::class, 'games_users');
    }

    public function getMainImage()
    {
        return $this->images()->where('isMain', '!=', NULL)->get()->first();
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }
}
