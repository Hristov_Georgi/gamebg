<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreMessage;
use App\Message;
use App\User;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $users = currentUser()->getConversationUsers();
        
        return view('messages.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMessage $request)
    {
        
        $data = $request->validated();
        
        // $message = new Message;
        // $message->sender_id = currentUser()->id;
        // $message->receiver_id = $data['receiver'];
        // $message->message = $data['message'];

        // $message->save();     

        Message::create([
            'sender_id' => currentUser()->id,
            'receiver_id' => $data['receiver'],
            'message' => $data['message'],
        ]);
        


        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function show($user) 
    {

        $user = User::where('username', $user)->get()->first(); //do it with autobind  
        
        $names = [
            currentUser()->id => currentUser()->username,
            $user->id => $user->username
        ];
        
        // dd($names);

        $conversation = currentUser()->getConversation($user);

        return view('messages.show',compact('names','user','conversation'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function edit(Message $message)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Message $message)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function destroy(Message $message)
    {
        //
    }
}
