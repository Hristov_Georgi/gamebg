<?php

namespace App\Http\Controllers\Admin;

use App\Game;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreGame;
use App\Http\Requests\StoreGameImage;
use App\Http\Requests\UpdateGame;
use App\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class GameController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    public function store(StoreGame $request)
    {
        
        // dd($request->all());
        $game = new Game();
        $game->name = $request->name;
        $game->description = $request->description;
        $game->slug = Str::slug($request->name,'-');
        $game->category_id = $request->categories;

        $game->save();

        $image = new Image();
        $path = $request->image->store('games/images');

        $image->path = '/storage/' . $path;
        $image->isMain = 1;
        $image->game_id = $game->id;

        $image->save();

        return back()->with('message','Game added successfuly');
    }

    public function update(UpdateGame $request, Game $game)
    {

        $data = $request->validated();
        // $request->session()->flash('editModal', $game->id);
        
        $game->name = $data['name'];
        $game->description = $data['description'];
        $game->category_id = $data['categories'];
   
        // if (array_key_exists('image',$data)) {
            
        //     $path = $data['image']->store('game_images');
            
        //     $image = new Image();
        //     $image->path = '/storage/' . $path;
        //     $image->game_id = $game->id;
            
        //     $image->save();
        // }

        $game->save();
        
        return back()->with('message','Game edited successfuly');
    }

    public function destroy(Game $game)
    {
        $game->delete();
        
        return back()->with('message', 'Deletion successful');
    }

    public function deleteImage(Request $request,Image $image)
    {
        if ($request->exists('removeImageBtn')) {
            $request->session()->flash('editModal', $image->game->id);
            
            $url = str_replace('/storage/','',$image->path);
            Storage::delete($url);
            $image->delete();
            
    
            return back()->with('message', 'image removed');
        }
        

    }

    public function setMainImage(Request $request, Image $image)
    {
        $request->session()->flash('editModal', $image->game->id);

        if ($request->exists('changeImageBtn') && ($image->isMain == null)) {
            
            if ($image->game->getMainImage()) {
                $oldCover = $image->game->getMainImage();
                $oldCover->isMain = null;
                $oldCover->save();
            }
            
            
            $image->isMain = 1;
            $image->save();

            return back()->with('message','Main image changed');
        }
        return back()->with('message','Already main image');
    }

    public function addImage(StoreGameImage $request)
    {
        
        $data = $request->validated();
        $url= $data['image']->store('game_images');
        $image = new Image();
        $image->path = '/storage/' . $url;
        $image->game_id = $data['gameId'];
        $image->save();
        
        $request->session()->flash('editModal', $image->game->id);

        return back()->with('message','Image added');
    }
}
