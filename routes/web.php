<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/test', 'TestController@index');
// Route::get('/fetch-categories', 'TestController@fetchCategories');

Route::get('/', 'HomeController@index')->name('home');

Route::middleware('auth')->group(function(){

    // Route::resource('profiles', 'ProfileController');
    // Route::resource('messages', 'MessageController');
    // Route::resource('friends', 'FriendsController');

    Route::resources([
        'profiles' => 'ProfileController',
        'messages' => 'MessageController',
        'friends' => 'FriendsController',
        'categories' => 'CategoryController'
    ]);
    
    
    Route::get('/profile/edit/pass', 'ProfileController@editPassword')->name('edit-pass');
    Route::put('/profile/{user}/change-pass', 'ProfileController@updatePassword')->name('update-pass');
    
    Route::put('/user/add-game/{game}', 'ProfileController@addGame')->name('add-game');
    Route::delete('/user/remove-game/{game}', 'ProfileController@removeGame')->name('del-game');
    
    Route::get('/profile/{user}/library', 'UserLibraryController')->name('user-game-lib');

    Route::post('/buddies', 'SearchBuddyController')->name('buddy');

    
    // Route::get('/profiles', 'ProfileController@index')->name('profiles');
    // Route::get('/profiles/{user}', 'ProfileController@show')->name('profiles.show');
    // Route::get('/profiles/{user}/edit', 'ProfileController@edit')->name('profiles.edit');
    // Route::put('/profiles/{user}', 'ProfileController@update')->name('profiles.edit');
    
    // Route::get('/friends', 'FriendsController@index')->name('friend-list');
    // Route::post('/friends/{user}', 'FriendsController@store')->name('add-friend');
    // Route::delete('friends/{user}', 'FriendsController@destroy')->name('remove-friend');
});

Route::prefix('admin')->name('admin.')->namespace('Admin')->group(function(){

    Route::get('/','HomeController')->name('index');
    Route::resource('games', 'GameController');
    Route::resource('users', 'UserController');
    Route::resource('categories', 'CategoryController');

    
    Route::post('games/image/', 'GameController@addImage')->name('add-image');
    Route::put('games/image/{image}', 'GameController@setMainImage')->name('set-main-image');
    Route::delete('games/image/{image}', 'GameController@deleteImage')->name('delete-image');
});

Route::resource('games','GameController')->only(['index','show']);

// Route::get('/games', 'GameController@index')->name('games');
// Route::get('/games/{game}', 'GameController@show')->name('games.show');


