<x-master>
    <div>
    @foreach ($users as $user)
        <div>
            <a href="{{ route('profiles.show',$user) }}">
                <p>{{ $user->username }}</p>
                <img width="35" src="{{ $user->avatar }}" alt="">
            </a>
        </div>
    @endforeach
    </div>
</x-master>