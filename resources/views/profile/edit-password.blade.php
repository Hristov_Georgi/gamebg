<x-profile>
    @if (session('message'))
    <div class="text-lg text-red-700">
        <p>{{ session('message') }}</p>
    </div>
    @endif
    <form method="POST" action="{{ route('update-pass',auth()->user()) }}">
        @csrf
        @method('PUT')


        <div class="nb-6">
            <label class="block mb-2 uppercase font-bold text-xs text-gray-600" for="currentPass">
                Current Password
            </label>
            <input class="border border-gray-400 p-2 w-full" type="password" name="currentPass">

            @error('currentPass')
            <p class="text-red-500 text-xs">{{ $message }}</p>
            @enderror
        </div>

        <div class="nb-6">
            <label class="block mb-2 uppercase font-bold text-xs text-gray-600" for="password">
                New Password
            </label>
            <input class="border border-gray-400 p-2 w-full" type="password" name="newPass">

            @error('newPass')
            <p class="text-red-500 text-xs">{{ $message }}</p>
            @enderror
        </div>

        <div class="nb-6">
            <label class="block mb-2 uppercase font-bold text-xs text-gray-600" for="password_confirm">
                Confirm New Password
            </label>
            <input class="border border-gray-400 p-2 w-full" type="password" name="newPass_confirmation">
        </div>


        <div class="nb-6">
            <button class="bg-red-400 text-white rounded py-2 px-4 mt-2" type="submit">
                <a href="{{ url()->previous() }}">Cancel</a>
            </button>

            <button class="bg-blue-400 text-white rounded py-2 px-4 mt-2" type="submit">
                Submit
            </button>
        </div>
    </form>

</x-profile>