<h1>Games</h1>

    <div>
        <table id="games_table" class="display">
            <thead>
                <tr>
                    <th>Main Cover</th>
                    <th>Name</th>
                    <th>Added on</th>
                    <th>Last Updated</th>
                    <th>Category</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($games as $game)
                <tr>
                    <td>
                        <x-display-image :game=$game></x-display-image>
                    </td>
                    <td>{{ $game->name }}</td>
                    @if ($game->created_at && $game->updated_at)
                        <td>{{ $game->created_at->diffForHumans() }}</td>
                        <td>{{ $game->updated_at->diffForHumans() }}</td>
                    @else
                        <td>{{ $game->created_at }}</td>
                        <td>{{ $game->updated_at }}</td>
                    @endif
                <td><a href="{{ route('categories.show', $game->category) }}">{{ $game->category->name }}</a></td>
                    <td>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#game-{{$game->id}}-modal">
                            Edit
                        </button>
                                        
                        <!-- Modal EDIT GAME-->
                        <div class="modal fade" id="game-{{$game->id}}-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Edit game data</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                </div>
                                <div class="modal-body">
                                    <form enctype="multipart/form-data" id="game-{{ $game->id }}" method="POST" action="{{ route('admin.games.update', $game) }}">
                                        @csrf
                                        @method('put')

                                        <label for="name">Change game name</label>
                                        <input type="text" name="name" value="{{ $game->name }}" class="border border-primary rounded" >
                                        <br>

                                        <label for="categories">Change category:</label>
                                        <select class="border border-primary rounded" name="categories" id="categories">
                                            @foreach ($categories as $category)
                                                <option value="{{ $category->id }}">{{ $category->name }}</option>

                                                @if ($game->category_id == $category->id)
                                                <option value="{{ $category->id }}" selected="selected" hidden >{{ $category->name }}</option>
                                                @endif
                                                
                                            @endforeach
                                        </select>

                                        <br>
                                        
                                        <label class="align-top" for="description">Change game description</label>
                                        <textarea class="border border-primary rounded" 
                                                name="description" 
                                                id="" cols="30" rows="4" 
                                                value>{{ $game->description }}
                                        </textarea>
                                        <br>
                                        
                                    </form>

                                    <div class="">
                                       
                                        <form action="{{ route('admin.add-image') }}" method="POST" enctype="multipart/form-data">
                                            @csrf
                                            <label for="image">Add image</label>
                                            <input type="hidden" name="gameId" value="{{ $game->id }}">
                                            <input type="file" name="image" >
                                            <input type="submit" value="Upload">
                                        <br>
                                        </form>
                                        @forelse ($game->images as $image)

                                            <img class="img-thumbnail" style="width:200px;height:200px;overflow:hidden" src="{{$image->path }}" alt="">
                                            @if ($image->isMain != NULL)
                                                <p>Main image</p>
                                            @endif

                                            
                                            <form action="{{ route('admin.set-main-image', $image) }}" method="post">
                                                @csrf
                                                @method('put')

                                                @if ($image->isMain == null)
                                                    <input type="submit" name="changeImageBtn" value="Make Main Image">
                                                @endif

                                            </form>
                                            <form action="{{ route('admin.delete-image', $image) }}" method="post">
                                                @csrf
                                                @method('delete')
                                                
                                                <input type="submit" name="removeImageBtn" value="Remove Image">
                                            </form>
                                        @empty
                                            <p>No images uploaded yet</p>
                                        @endforelse
                                    </div>
                                </div>
                                <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <button type="submit" form="game-{{ $game->id }}" class="btn btn-primary">Edit Game</button>
                                </div>
                            </div>
                            </div>
                        </div>
                    </td>
                    <td>
                        <form method="POST" action="{{ route('admin.games.destroy', $game) }}">
                            @csrf
                            @method('delete')

                            <button type="submit">
                                DELETE
                            </button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>


    <!-- Button trigger modal -->
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addGameModal">
        Add New Game
    </button>
    
    <!-- Modal Add game-->
    <div class="modal fade" id="addGameModal" tabindex="-1" role="dialog" aria-labelledby="gameLabelModal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title" id="gameLabelModal">Enter game data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="modal-body">
                    <form enctype="multipart/form-data" id="add_game_form" method="POST" action="{{ route('admin.games.store') }}">
                        @csrf
                        <label for="n">Game name</label>
                        <input type="text" name="name" id="n" class="border border-primary" >
                        <br>
                        <label class="align-top">Description</label>
                        <textarea class="border border-primary rounded" 
                            name="description" 
                            id="" cols="30" rows="4" 
                            value
                            ></textarea>
                            
                        <br>
                        <br>
                        <label>Choose category</label>
                        <select class="border border-primary rounded" name="categories">
                            @foreach ($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                            @endforeach
                        </select>
                        <br>
                        <label for="i">Main image/cover</label>
                        <input type="file" name="image" id="i" > 
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" form="add_game_form" class="btn btn-primary">Add game</button>
                </div>
            </div>
        </div>
    </div>
   
    <input type="hidden" id="gameId" value="{{ Session::get('editModal','index') }}"></input>