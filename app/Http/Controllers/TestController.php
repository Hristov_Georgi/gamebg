<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\User;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public function index()
    {
        // $categories = new CategoryResource(Category::first());
    
        // dd('in controller');

        return view('test');
        // return $categories;
    }

    public function fetchCategories()
    {
        $categories = new CategoryResource(Category::first());
        return $categories;
    }
 }
