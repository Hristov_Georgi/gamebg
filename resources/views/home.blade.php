<x-master>

    <div class="flex justify-center mb-5">
        <a class="text-xl bg-orange-500" href="{{ route('games.index') }}">Explore All games</a>
    </div>
    <div class="grid grid-cols-3 gap-10">

        @forelse ($categories as $category)

        <div class="">
            <h2>{{ $category->name }}</h2>
            <a href="{{ route('categories.show',$category) }}">
                <img width="150" src="{{ $category->image }}" alt="{{ $category->name }}'s image'">
            </a>
        </div>

        @empty
        <p>No categories to show</p>
        @endforelse
    </div>

</x-master>