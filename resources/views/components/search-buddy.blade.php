<div class="flex flex-col bg-blue-200">
    <h1>Search Buddy by game</h1>

    <label for="games">Choose games:</label>

    <form action="{{ route('buddy') }}" method="POST">
        @csrf
        <select name="values[]" id="games" multiple>
            @foreach (App\Game::all() as $game)
                <option value="{{ $game->id }}">{{ $game->name}}</option>
            @endforeach
    
        </select>
        <button class="block" type="submit">Search</button>
    </form>
</div>