GameBG

Laravel 7.x Project



Quick Start

# Install Dependencies
composer install

# Run Migrations
php artisan migrate

# Import Articles
php artisan db:seed

# Add virtual host if using Apache

# If you get an error about an encryption key
php artisan key:generate




This is the repository for the GAMEBG proj

Make a social network for gamers. This is the place where gamers would come and find buddies to play their favorite games with.

The app should consist of the following functionalities:

    Basic authentication system
        Login - with username and password
        Register - username, name, email, password, repeat password, image (optional)
            Validations: 
                The username must be unique and fit into a varchar field
                The name must be filled and fit into a varchar field
                The email must be filled, be a valid email and fit into a varchar field
                The passwords must match and be at least 6 symbols
                The image is optional, but if it filled it should be a valid image format
        Reset password
    Categories
        Each category has a name, image and games. They must be stored in the DB, but users will not be able to manage them.
        Use seeders, factories and faker, to generate them.
    Games
        Each game has a category, name, images (one of which should be marked as a main image to use in listings, etc.), description. 
        If a user is logged in, there must be an "I play this" button, which will add the game to the user's list of games he plays.
        "People who play this game" link. It must lead to the "Search buddy" page with the appropriate search params.
        Use seeders, factories and faker, to generate them.
    User profile
        Account
            Data - A page where the user will be able to edit all his data that he entered when registering.
                Change password - Seperate from the data edit. The user needs to enter the new password and confirm it. The user must enter current password to authorize the password change.
            Games - All games that the user plays. There should be a remove button on each game.
    Search for a buddy
        A search where you select a few games fom the list of games in the app and it shows you all the users that play them. You should show which games from the ones you are searching are matching.
        Example: you search for people who play Diablo 3 and Starcraft 2. The results will contain some users who play Starcraft 2, but do not play Diablo 3 and vice versa. That's why you must show next to the users, the reason why are they matching your search.
    View user profile
        The user profile must show
            Username
            Games they play
            Message option
            Add friend option
    My friends
        Show a list of users you added as your friends
    Messages
        You must be able to message every user. You will be able to message them from a button in their profile, or from your friends listing.
        Every user must be able to check their messages and answer them.

Admin panel

A panel which will reside on a special link which will not be accessible by any hrefs on the main site. It will also require a login, but the user must be an admin to access it. Hint: this should be done with a middleware and a user-role system.

    Categories - Basic CRUD for the categories with proper validations.
    Games - Basic CRUD for the games with proper validations.
    Users - A list of all users with view and delete options. The table should show a username, email, how many games he plays and since when he is using the app. Check this https://carbon.nesbot.com/docs/#api-humandiff
    Bonus: 
        Use datatables
        Make the admin part of the app different from the front, so that you can know that you are in admin. Even if you change just some colors.
        Add slugs to games and categories and use it as a url parameter insted of the ID. Don't forget the the slugs must be unique.
        Use the user "username" in the url instead of the ID.

The biggest hint of all:

If you see something among these lines for the very first time in your life and don't know anything about it, Google it.
