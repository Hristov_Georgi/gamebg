<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Game;
use App\Image;
use Faker\Generator as Faker;

$factory->define(Image::class, function (Faker $faker) {
    return [
        'path' => '/storage/games/images/'. $faker->file(
            'public/app_files/game_covers',
            'storage/app/public/games/images',
    false),
        'game_id' => Game::orderByRaw('RAND()')->first()->id,
    ];
});


