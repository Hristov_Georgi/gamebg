<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateUserProfile extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => [
                'required',
                'string',
                'alpha_dash',
                Rule::unique('users')->ignore(currentUser()),
            ],
            'name' => 'string|max:255|min:4',
            'email' => [
                'required',
                'email',
                Rule::unique('users')->ignore(currentUser()),
            ],
            'avatar' => 'file|image'
        ];
    }
}
