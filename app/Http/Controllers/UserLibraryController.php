<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserLibraryController extends Controller
{
    public function __invoke(User $user)
    {
        return view('profile.library', compact('user'));
    }
}
