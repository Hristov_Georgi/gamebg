<x-profile>
    <div  class="flex flex-col bg-gray-200">
        <h1>Your message history with {{ $user->username }}</h1>
        <div>
            @php
            $tmp = $conversation->first()->sender_id;
            @endphp

            @foreach ($conversation as $reply)
                <div class="flex">
                    @if ($loop->first)
                            <h2>{{ $names[$reply->sender_id] }} -->> </h2>
                        @php
                            $tmp = $names[$reply->sender_id];
                        @endphp
                    
                    @endif

                    @if ($names[$reply->sender_id] != $tmp )
                        <h2>{{ $names[$reply->sender_id] }} -->> </h2>
                        <p>{{ $reply->message }}</p>
                        @php
                            $tmp = $names[$reply->sender_id];
                        @endphp
                    @else
                        <p>>>{{ $reply->message }}</p>
                    @endif
                </div>
            @endforeach
        </div>
        <div>
            <h2>SEND MESSAGE</h2>
            <form method="POST" action="{{ route('messages.store') }}">
                @csrf
                <textarea class="border border-orange-700" name="message" cols="10" rows="3"></textarea>
                <input type="hidden" value="{{ $user->id }}" name="receiver" id="">
                <button type="submit">SUBMIT PM</button>
            </form>
        </div>
    </div>
</x-profile>