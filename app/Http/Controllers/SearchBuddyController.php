<?php

namespace App\Http\Controllers;

use App\Game;
use App\Http\Requests\ShowSearchBuddy;
use App\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class SearchBuddyController extends Controller
{
    public function __invoke(ShowSearchBuddy $request)
    {
        $vals = $request->validated();

        $matches = Game::whereIn('id', $vals)->pluck('name', 'id');
        $games = Game::whereIn('id', $vals)->get();

        $userNames = collect();

        foreach ($games as $game) {

            foreach ($game->user as $key => $user) {
               if (!($userNames->contains($user->username))) {
                   $userNames->push($user->username);
               }
            }
        }
       
        // dd($matches, $userNames);

        $users = User::whereIn('username',$userNames)->get();
        // dd($matches);

        return view('buddies', compact('users', 'matches'));
    }
}
