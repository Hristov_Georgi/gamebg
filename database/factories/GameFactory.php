<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Category;
use App\Game;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Game::class, function (Faker $faker) {
    
    $sentence = $faker->sentence(3);
    $slug = Str::slug($sentence,'-');

    return [
        'name' => $sentence,
        // 'image' => $faker->imageUrl(),
        'description' => $faker->paragraph(2),
        'slug' => $slug,
        // 'category_id' => factory(Category::class)
        'category_id' => Category::orderByRaw('RAND()')->first()->id
    ];
});
