<div>

    <div class="my-5 mx-3">

        <div class="flex justify-end">

            <div class="">
                <a class="bg-red-500 mt-4 rounded-lg shadow py-2 px-2 text-white" href="{{ route('logout') }}" onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="">
                    @csrf
                </form>
            </div>

        </div>


        <div>
            <div>
                <a href="{{ route('friends.index') }}">Friends: {{ currentUser()->friends->count()}} </a>
            </div>
        </div>


        <div class="flex">

            <a class="text-xl" href="{{ route('profiles.show',auth()->user()) }}">
                Welcome, {{ Auth::user()->name }}!
            </a>
            <div>
                <img src="{{ currentUser()->avatar }}" alt="" class="rounded-full mr-2 " width="50">
            </div>
        </div>

    </div>

</div>