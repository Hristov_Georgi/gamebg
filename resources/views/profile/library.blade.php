<x-profile>
    @if ($user->is(currentUser()))
        <h1>Your game library</h1>
    @else
        <h1>{{ $user->username }}'library</h1>
    @endif
    <div class="grid grid-cols-3 gap-4">
        @forelse ($user->games as $game)
            <div class="bg-gray-400">
                <form action="{{ route('del-game', $game) }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button type="submit">
                        Remove game
                    </button>
                </form>
                <h3>{{ $game->name }}</h3>
                <img src="{{ $game->image }}" 
                     alt="{{ $game->name }}'s main image'">
                
            </div>
        @empty
            <h2>You dont have any added games yet!</h2>
        @endforelse
    </div>
</x-profile>