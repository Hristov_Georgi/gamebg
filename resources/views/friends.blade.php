<x-master>
    @forelse ($user->friends as $friend)
        <div>
            <a href="{{ route('profiles.show',$friend->username) }}"><h2>{{ $friend->name }}</h2></a>

            
            <img class="rounded-full mr-2 " width="50" 
            src="{{ $friend->avatar }}" 
            alt="{{ $friend->username }}'s avatar"
            >

        </div>
    @empty
        <p>You don't have any friends yet</p>
    @endforelse
</x-master>